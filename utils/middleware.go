package utils

import (
	"net/http"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"
)

// Claim struct
type Claim struct {
	UuID string `json:"uuid"`
	jwt.StandardClaims
}

var jwtKey = []byte(uuid.NewV4().String())

// Middleware handler
func Middleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		_, token, err := ParseToken(c.Request)
		if err != nil {
			if err == jwt.ErrSignatureInvalid {
				c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"message": "invalid-token"})
				return
			}

			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"message": "unauthorized"})
			return
		}
		if !token.Valid {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"message": "invalid-token"})
			return
		}
		c.Next()
	}
}

// GenerateToken func
func GenerateToken(uuid string) (string, int64, error) {
	expirationTime := time.Now().Add(time.Hour * 24).Unix()
	claims := &Claim{
		UuID: uuid,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime,
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		return "", 0, err
	}

	return tokenString, expirationTime, nil
}

// ParseToken func
func ParseToken(r *http.Request) (Claim, *jwt.Token, error) {
	tokenStr := strings.Split(string(r.Header.Get("Authorization")), " ")
	if len(tokenStr) < 2 {
		return Claim{}, nil, jwt.ErrSignatureInvalid
	}
	claims := Claim{}
	token, err := jwt.ParseWithClaims(tokenStr[1], &claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	if err != nil {
		return Claim{}, nil, err
	}
	return claims, token, nil
}

// GetOriginator func
func GetOriginator(r *http.Request) (Claim, error) {
	claims, _, err := ParseToken(r)
	return claims, err
}
