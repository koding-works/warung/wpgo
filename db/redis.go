package db

import (
	"wpgo/utils"

	"github.com/alicebob/miniredis"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
)

// InitRedis func
func InitRedis() (conn *redis.Client) {
	conf := getConfig()

	if utils.IsEqual(gin.Mode(), gin.TestMode) {
		s, err := miniredis.Run()
		if err != nil {
			panic(err)
		}

		conn = redis.NewClient(&redis.Options{
			Addr:     s.Addr(),
			Password: "",
			DB:       0,
		})
		return
	}

	conn = redis.NewClient(
		&redis.Options{
			Addr: conf.RedisHost + ":" + conf.RedisPort,
			DB:   0,
		},
	)
	return
}
