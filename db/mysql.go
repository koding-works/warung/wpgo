package db

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

// InitMysql func
func InitMysql() (conn *sql.DB) {
	conf := getConfig()
	var err error

	if conn, err = sql.Open(
		"mysql",
		fmt.Sprintf("%s:%s@tcp(%s:%d)/%s",
			conf.User,
			conf.Password,
			conf.Host,
			conf.Port,
			conf.DbName,
		),
	); err != nil {
		panic(err)
	}
	return
}
