VERSION_TAG ?= dev
DIND_PREFIX ?= $(HOME)
ifneq ($(HOST_PATH),)
DIND_PREFIX := $(HOST_PATH)
endif
ifeq ($(CACHE_PREFIX),)
	CACHE_PREFIX=/tmp
endif

PREFIX=$(shell echo $(PWD) | sed -e s:$(HOME):$(DIND_PREFIX):)
export DIND_PATH=$(PREFIX)

serve:
	@go run cmd/wpgo/main.go

local_test:
	@GIN_MODE=test go test --cover ./...

prep_local_test: clean_test
	@docker-compose -f deployments/docker-compose.ut.yaml up -d --force-recreate local_testdb
	@sleep 10
	@docker exec wpgo_local_db_test /restore.sh

test: clean_test build_test
	@echo "Starting test..."
	@docker-compose -f deployments/docker-compose.ut.yaml up -d --force-recreate db
	@docker-compose -f deployments/docker-compose.ut.yaml up -d --force-recreate redis
	@sleep 10
	@docker exec wpgo_db_test /restore.sh
	@docker-compose -f deployments/docker-compose.ut.yaml run wpgo /start.sh
	@docker-compose -f deployments/docker-compose.ut.yaml down -v

clean_test:
	@docker-compose -f deployments/docker-compose.ut.yaml stop
	@docker-compose -f deployments/docker-compose.ut.yaml rm -f local_testdb
	@docker-compose -f deployments/docker-compose.ut.yaml rm -f db
	@docker-compose -f deployments/docker-compose.ut.yaml rm -f redis
	@docker-compose -f deployments/docker-compose.ut.yaml rm -f wpgo

build_test:
	@docker-compose -f deployments/docker-compose.ut.yaml build wpgo

migrate_clean:
	@ echo "Cleaning migrations..."
	@rm migrations/deploy/*.sql

migrate:
	@docker run --rm -v ${PWD}/migrations/deploy:/migrations --network host migrate/migrate\
    	-path=/migrations/ -database postgres://$(DB_USER):$(DB_PASS)@$(DB_HOST):$(DB_PORT)/$(DB_NAME)?sslmode=disable up

native_migrate:
	@migrate -path migrations/deploy/ \
		-database "postgres://$(DB_USER):$(DB_PASS)@$(DB_HOST):$(DB_PORT)/$(DB_NAME)?sslmode=disable" up

build_wpgo:
	mkdir -p ./build
	chmod 755 ./scripts/build.sh
	docker run \
		-v $(PREFIX)/build:/build \
		-v $(CACHE_PREFIX)/cache/go:/go/pkg/mod \
		-v $(CACHE_PREFIX)/cache/apk:/etc/apk/cache \
		-v $(PREFIX)/cmd:/go/src/wpgo/cmd \
		-v $(PREFIX)/:/host \
		-v $(PREFIX)/scripts/build.sh:/build.sh golang:1.12.7-alpine /bin/sh /build.sh

	docker build \
		--build-arg GOPROXY \
		--build-arg VERSION_TAG=$(VERSION_TAG) \
		--build-arg GIN_MODE=release \
		-t kodingworks/registry/wpgo-v1:$(VERSION_TAG) .

artifacts:
	@mkdir -p bundle
	@mkdir -p wpgo
	
	docker save -o ./wpgo/kodingworks-wpgo-v1-$(VERSION_TAG).tar kodingworks/registry/wpgo-v1:$(VERSION_TAG)
	cp -rf deployments/docker-compose.template.yaml wpgo/docker-compose.yaml
	echo "VERSION_TAG=$(VERSION_TAG)" >> ./wpgo/.env
	echo "DB_HOST=" >> ./wpgo/.env
	echo "DB_PORT=" >> ./wpgo/.env
	echo "DB_NAME=" >> ./wpgo/.env
	echo "DB_USER=" >> ./wpgo/.env
	echo "DB_PASS=" >> ./wpgo/.env

	# Bundle and release
	@echo "Bundling..."
	@tar czf kodingworks-wpgo-v1-$(VERSION_TAG).tar.gz wpgo
	@mv kodingworks-wpgo-v1-$(VERSION_TAG).tar.gz bundle
	@rm -rf wpgo
	@echo "New release with version tag: $(VERSION_TAG)"