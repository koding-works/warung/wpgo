package orders

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
	"wpgo/db"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

var api *OrderAPI
var resp = httptest.NewRecorder()
var req *gin.Context
var route *gin.Engine

func TestInit(t *testing.T) {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	req, route = gin.CreateTestContext(resp)

	api = &OrderAPI{
		DB:    db.InitMysql(),
		Redis: db.InitRedis(),
	}
}

func TestOrdersListRequest(t *testing.T) {
	route.GET("/orders", api.ordersListRequest)

	req.Request, _ = http.NewRequest(http.MethodGet, "/orders", nil)
	route.ServeHTTP(resp, req.Request)

	assert.Equal(t, http.StatusOK, resp.Result().StatusCode, "Expected 200 status code")

	var data map[string]interface{}

	res, _ := ioutil.ReadAll(resp.Body)
	err := json.Unmarshal(res, &data)
	assert.Equal(t, nil, err, "Expected no error")
	assert.Equal(t, float64(1), float64(len(data["orders"].([]interface{}))), "Expected got 1 orders")

	// Test per page
	req.Request, _ = http.NewRequest(http.MethodGet, "/orders?per_page=10&page=1", nil)
	route.ServeHTTP(resp, req.Request)

	assert.Equal(t, http.StatusOK, resp.Result().StatusCode, "Expected 200 status code")

	res, _ = ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(res, &data)
	assert.Equal(t, nil, err, "Expected no error")
	assert.Equal(t, float64(1), float64(len(data["orders"].([]interface{}))), "Expected got 1 orders")

	// Test filter
	req.Request, _ = http.NewRequest(http.MethodGet, "/orders?per_page=10&page=1&email=07ariaseta@gmail.com&status=processing", nil)
	route.ServeHTTP(resp, req.Request)

	assert.Equal(t, http.StatusOK, resp.Result().StatusCode, "Expected 200 status code")

}
