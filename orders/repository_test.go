package orders

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAllOrders(t *testing.T) {
	meta := ordersMeta{
		Limit: 10,
		Page:  1,
	}

	results, err := api.all(meta)
	assert.Equal(t, nil, err, "Expected no error")
	assert.Equal(t, true, len(results.Data) > 0, "Expected got result")

	// Filter by wc_status
	meta.params = map[string]string{
		"wc_status": "wc-pending", // Random status
	}
	results, err = api.all(meta)
	assert.Equal(t, nil, err, "Expected no error")
	assert.Equal(t, true, len(results.Data) > 0, "Expected got result")

	// Filter by email
	meta.params = map[string]string{
		"email": "07ariaseta@gmail.com",
	}
	results, err = api.all(meta)
	assert.Equal(t, nil, err, "Expected no error")
}
