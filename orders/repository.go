package orders

import (
	"database/sql"
	"fmt"
	"log"
	"strconv"
	"wpgo/utils"

	"github.com/go-sql-driver/mysql"
)

func (api *OrderAPI) all(meta ordersMeta) (results ordersMeta, err error) {
	query := `
		SELECT
			p.ID,
			p.post_date AS 'date',
			p.post_status AS 'status',
			MAX( CASE WHEN pm.meta_key = '_billing_first_name' and p.ID = pm.post_id THEN pm.meta_value END ) AS first_name,
			MAX( CASE WHEN pm.meta_key = '_billing_last_name' and p.ID = pm.post_id THEN pm.meta_value END ) AS last_name,
			MAX( CASE WHEN pm.meta_key = '_billing_address_1' and p.ID = pm.post_id THEN pm.meta_value END ) AS address,
			MAX( CASE WHEN pm.meta_key = '_billing_phone' and p.ID = pm.post_id THEN pm.meta_value END ) AS phone,
			MAX( CASE WHEN pm.meta_key = '_order_total' and p.ID = pm.post_id THEN pm.meta_value END ) AS price
		FROM
			wp_posts p 
			JOIN wp_postmeta pm ON p.ID = pm.post_id
			JOIN wp_postmeta AS pm1 ON pm.post_id = pm1.post_id
		WHERE
			post_type = 'shop_order'
	`

	/* filter by wc_status */
	if !utils.IsEmpty(meta.params["wc_status"]) {
		query += fmt.Sprintf(" AND post_status IN ('%v') ", meta.params["wc_status"])
	}

	/* filter by email */
	if !utils.IsEmpty(meta.params["email"]) {
		query += fmt.Sprintf(" AND pm1.meta_key = '_billing_email' AND pm1.meta_value = '%v' ", meta.params["email"])
	}

	query += " GROUP BY p.ID "

	if err = api.DB.QueryRow(
		fmt.Sprintf("SELECT COUNT(*) FROM (%s) AS count", query),
	).Scan(&meta.Total); err != nil && err != sql.ErrNoRows {
		log.Println("Query(): ", err.Error())
		return
	}

	meta.OrderBy = "post_date"
	query += fmt.Sprintf("ORDER BY p.%s DESC LIMIT %d OFFSET %d", meta.OrderBy, meta.Limit, (meta.Page-1)*meta.Limit)

	var rows *sql.Rows
	rows, err = api.DB.Query(query)
	if err != nil && err != sql.ErrNoRows {
		log.Println("Query(): ", err.Error())
		return
	}
	defer rows.Close()

	data := make([]orders, 0)
	for rows.Next() {
		var order orders
		var ID sql.NullInt64
		var firstName, lastName, address, status, phone, price sql.NullString
		var date mysql.NullTime

		if err = rows.Scan(
			&ID,
			&date,
			&status,
			&firstName,
			&lastName,
			&address,
			&phone,
			&price,
		); err != nil {
			log.Println("Scan(): ", err.Error())
			return
		}
		order.ID = int(ID.Int64)
		order.Date = date.Time
		order.Status = status.String
		order.FirstName = firstName.String
		order.LastName = lastName.String
		order.Address = address.String
		order.Phone = phone.String

		if order.Price, err = strconv.Atoi(price.String); err != nil {
			err = nil
			order.Price = 0
		}

		data = append(data, order)
	}
	meta.Data = data
	results = meta
	return
}

func (api *OrderAPI) get() (order orders, err error) {
	return
}

func (api *OrderAPI) add() (err error) {
	return
}

func (api *OrderAPI) update() (err error) {
	return
}

func (api *OrderAPI) delete() (err error) {
	return
}
