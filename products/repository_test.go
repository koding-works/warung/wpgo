package products

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAllProducts(t *testing.T) {
	meta := productsMeta{
		Limit: 10,
		Page:  1,
	}

	results, err := api.all(meta)
	assert.Equal(t, nil, err, "Expected no error")
	assert.Equal(t, true, len(results.Data) > 0, "Expected got result")

	// Filter by category
	meta.params = map[string]string{
		"category_id": "27", // Random id
	}
	results, err = api.all(meta)
	assert.Equal(t, nil, err, "Expected no error")
	assert.Equal(t, true, len(results.Data) > 0, "Expected got result")

	// Filter by vendor
	meta.params = map[string]string{
		"vendor_id": "19", // Random id
	}
	results, err = api.all(meta)
	assert.Equal(t, nil, err, "Expected no error")
	assert.Equal(t, true, len(results.Data) > 0, "Expected got result")

	// Filter by search keyword
	meta.params = map[string]string{
		"post_title": "bayam", // Random id
	}
	results, err = api.all(meta)
	assert.Equal(t, nil, err, "Expected no error")
	assert.Equal(t, true, len(results.Data) > 0, "Expected got result")
}
