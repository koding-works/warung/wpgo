package products

import (
	"database/sql"
	"log"
	"net/http"
	"strconv"
	"wpgo/utils"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
)

type ProductAPI struct {
	Router *gin.RouterGroup
	DB     *sql.DB
	Redis  *redis.Client
}

func (api *ProductAPI) Register() {
	api.Router.GET("/products", api.productsListRequest)

	log.Println("Products API Registered")
}

func (api *ProductAPI) productsListRequest(c *gin.Context) {
	var meta productsMeta
	var err error
	params := make(map[string]string, 0)

	if meta.Limit, err = strconv.Atoi(utils.Sanitize(c.DefaultQuery("per_page", "10"))); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "invalid-limit-type"})
		return
	}
	if meta.Page, err = strconv.Atoi(utils.Sanitize(c.DefaultQuery("page", "1"))); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "invalid-page-type"})
		return
	}
	if !utils.IsEmpty(c.Query("category_id")) {
		params["category_id"] = utils.Sanitize(c.Query("category_id"))
	}
	if !utils.IsEmpty(c.Query("vendor_id")) {
		params["vendor_id"] = utils.Sanitize(c.Query("vendor_id"))
	}
	if !utils.IsEmpty(c.Query("search")) {
		params["post_title"] = utils.Sanitize(c.Query("search"))
	}
	meta.params = params

	result, err := api.all(meta)
	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, result)
}
