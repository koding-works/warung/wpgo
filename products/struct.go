package products

type products struct {
	ID           int    `json:"id"`
	Name         string `json:"mame"`
	CategoryID   int    `json:"category_id"`
	CategoryName string `json:"category_name"`
	VendorID     int    `json:"vendor_id"`
	VendorName   string `json:"vendor_name"`
	StockStatus  string `json:"stock_status"`
	Price        int    `json:"price"`
	RegularPrice int    `json:"regular_rice"`
	SalePrice    int    `json:"sale_price"`
	Description  string `json:"description"`
	ThumbnailURL string `json:"thumbnail_url"`
}

type productsMeta struct {
	Data    []products `json:"products"`
	Page    int        `json:"page"`
	Limit   int        `json:"limit"`
	Total   int        `json:"total"`
	OrderBy string     `json:"order_by"`

	// Unexposed field
	params map[string]string
}
