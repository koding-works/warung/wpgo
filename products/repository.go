package products

import (
	"database/sql"
	"fmt"
	"log"
	"strconv"
	"wpgo/utils"
)

func (api *ProductAPI) all(meta productsMeta) (results productsMeta, err error) {
	query := `
		SELECT
			p.ID AS id,
			p.post_title AS name,
			category.term_id as category_id,
			category.name as category_name,
			vendor.term_id as vendor_id,
			vendor.name as vendor_name,
			stock_status.meta_value as stock_status,
			price.meta_value as price,
			regular_price.meta_value as regular_price,
			sale_price.meta_value as sale_price,
			p.post_content as description,
			thumbnail_url.guid as thumbnail_url
		FROM wp_posts p
		LEFT JOIN wp_postmeta AS stock_status 
				ON p.ID = stock_status.post_id AND stock_status.meta_key='_stock_status'
		LEFT JOIN wp_postmeta  AS price 
				ON p.ID = price.post_id AND price.meta_key='_price'
		LEFT JOIN wp_postmeta  AS regular_price 
			ON p.ID = regular_price.post_id AND regular_price.meta_key='_regular_price'
		LEFT JOIN wp_postmeta  AS sale_price 
			ON p.ID = sale_price.post_id AND sale_price.meta_key='_sale_price'
		LEFT JOIN wp_postmeta  AS thumbnail_id 
			ON p.ID = thumbnail_id.post_id  AND thumbnail_id.meta_key='_thumbnail_id'
		LEFT JOIN wp_posts  AS thumbnail_url 
			ON thumbnail_id.meta_value = thumbnail_url.ID   
		INNER JOIN wp_term_relationships AS wtr3 
			ON p.ID = wtr3.object_id
		INNER JOIN wp_term_taxonomy AS wtt3 
			ON wtr3.term_taxonomy_id = wtt3.term_taxonomy_id  AND wtt3.term_taxonomy_id IN 
			(
				SELECT 
					term_taxonomy_id
				FROM
					wp_term_taxonomy
				WHERE
					taxonomy = 'product_cat'
			)
		INNER JOIN wp_terms AS category 
			ON wtt3.term_id = category.term_id	
		INNER JOIN wp_term_relationships AS wtr2
			ON p.ID = wtr2.object_id
		INNER JOIN wp_term_taxonomy AS wtt2 
			ON wtr2.term_taxonomy_id = wtt2.term_taxonomy_id AND wtt2.taxonomy = 'wcpv_product_vendors'	
		INNER JOIN wp_terms AS vendor 
			ON wtt2.term_id = vendor.term_id
		WHERE
			p.post_type = 'product'
			AND p.post_status = 'publish'
	`

	/* filter by category_id */
	if !utils.IsEmpty(meta.params["category_id"]) {
		query += fmt.Sprintf("AND category.term_id = %v ", meta.params["category_id"])
	}

	/* filter by vendor_id */
	if !utils.IsEmpty(meta.params["vendor_id"]) {
		query += fmt.Sprintf(" AND wtt2.term_id = %v ", meta.params["vendor_id"])
	}

	/* serach by keyword */
	if !utils.IsEmpty(meta.params["post_title"]) {
		query += " AND p.post_title LIKE '%" + meta.params["post_title"] + "%' "
	}

	if err = api.DB.QueryRow(
		fmt.Sprintf("SELECT COUNT(*) FROM (%s) AS count", query),
	).Scan(&meta.Total); err != nil && err != sql.ErrNoRows {
		log.Println("Query(): ", err.Error())
		return
	}

	meta.OrderBy = "post_title"
	query += fmt.Sprintf("ORDER BY p.%s LIMIT %d OFFSET %d", meta.OrderBy, meta.Limit, (meta.Page-1)*meta.Limit)

	var rows *sql.Rows
	rows, err = api.DB.Query(query)
	if err != nil && err != sql.ErrNoRows {
		log.Println("Query(): ", err.Error())
		return
	}
	defer rows.Close()

	data := make([]products, 0)
	for rows.Next() {
		var product products
		var ID, categoryID, vendorID sql.NullInt64
		var name, categoryName, vendorName, stockStatus, price sql.NullString
		var regularPrice, salePrice, description, thumbnailURL sql.NullString

		if err = rows.Scan(
			&ID,
			&name,
			&categoryID,
			&categoryName,
			&vendorID,
			&vendorName,
			&stockStatus,
			&price,
			&regularPrice,
			&salePrice,
			&description,
			&thumbnailURL,
		); err != nil {
			log.Println("Scan(): ", err.Error())
			return
		}
		product.ID = int(ID.Int64)
		product.CategoryID = int(categoryID.Int64)
		product.CategoryName = categoryName.String
		product.VendorID = int(vendorID.Int64)
		product.VendorName = vendorName.String
		product.StockStatus = stockStatus.String

		if product.Price, err = strconv.Atoi(price.String); err != nil {
			err = nil
			product.Price = 0
		}

		if product.RegularPrice, err = strconv.Atoi(regularPrice.String); err != nil {
			err = nil
			product.RegularPrice = 0
		}

		if product.SalePrice, err = strconv.Atoi(salePrice.String); err != nil {
			err = nil
			product.SalePrice = 0
		}

		product.Description = description.String
		product.ThumbnailURL = thumbnailURL.String

		data = append(data, product)
	}
	meta.Data = data
	results = meta
	return
}

func (api *ProductAPI) get() (product products, err error) {
	return
}

func (api *ProductAPI) add() (err error) {
	return
}

func (api *ProductAPI) update() (err error) {
	return
}

func (api *ProductAPI) delete() (err error) {
	return
}
