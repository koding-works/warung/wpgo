package auth

import (
	"database/sql"
	"log"
	"wpgo/utils"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
)

type AuthAPI struct {
	Router *gin.RouterGroup
	DB     *sql.DB
	Redis  *redis.Client
}

func (api *AuthAPI) Register() {
	api.Router.POST("/auth/login", api.loggedInRequest)
	api.Router.Use(utils.Middleware()).POST("/auth/logout", api.logggedOutRequest)

	log.Println("Auth API Registered")
}

func (api *AuthAPI) loggedInRequest(c *gin.Context) {

}

func (api *AuthAPI) logggedOutRequest(c *gin.Context) {

}
