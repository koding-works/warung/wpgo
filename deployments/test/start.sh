#!/bin/sh
set -e
set -x

/wait-for-it.sh $DB_HOST:$DB_PORT -- echo "Starting..."

go test --cover ./...