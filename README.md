# Wordpress GO (WPGO)
WPGO is a project to get data from wordpress database directly.

## Description (Objective), why we build this thing
We build this tools for communication between wordpress database and third party, so we can protect the data with not directly access the wordpress database.

## Tech Stack Used
- Using GO 1.12.17 or higer
- Using GIN framework
- Using native GO database/sql driver
- Using build essentials to make a custom command 
- Using docker to deployment

## Folder Structure
```
.
├── Dockerfile
├── Makefile
├── README.md
├── auth
│   ├── api.go
│   ├── api_test.go
│   ├── repository.go
│   ├── repository_test.go
│   └── struct.go
├── cmd
│   └── wpgo
│       ├── main.go
│       └── wpgo
├── configs
│   └── config.json
├── db
│   ├── helpers.go
│   ├── mysql.go
│   ├── postgres.go
│   └── redis.go
├── deployments
│   └── docker-compose.template.yaml
├── go.mod
├── go.sum
├── migrations
│   └── 0_init.sql
├── posts
│   ├── api.go
│   ├── api_test.go
│   ├── repository.go
│   ├── repository_test.go
│   └── struct.go
├── scripts
│   ├── build.sh
│   ├── start.sh
│   └── wait-for-it.sh
└── utils
    ├── exts.go
    └── middleware.go
```

- `Dockerfile`  <- Dockerfile 
- `Makefile`    <- Put custom make command here
- `README.md`   <- Readme
- `auth`        <- Example one of modules same with `posts`
- `cmd`         <- Here is go main package
- `configs`     <- Put the local serve configuration here
- `db`          <- Put the database setup here
- `deployments` <- All of deployments setup here and don't forgot to add in our bundle artifacts
- `go.mod`      <- Go modules
- `go.sum`      <- Go modules
- `migrations`  <- Here we collect all of migrations from each module
- `scripts`     <- Here is all of shell script
- `utils`       <- Put your code which you think can be reuse anytime

## How To Add New WPGO Modules ?
```
Easy way to do this, see the existing module for the structure and code formating and adjust to your need.
Adding new modules should be registered two places. in `main.go` and `build.sh` script. 
```

## How to Run
- Download and install wordpress to your local machine, we need the database from it
- Adjust `./configs/config.json` to your local databases
- Use `make serve` to run the app, please see the `CMD` for more detail

## How to Build and Deploy
- WPGO is build in ci/cd and bundling it into artifacts
- Download the artifacts from `Build production` job
- Place it on your server, and extract it <prod-release>.tar.gz
- Adjust `.env` and `docker-compose.yaml` file to your need

## CMD
- `make serve`          <- Run the wpgo app
- `make test`           <- Run the test with docker. the test environment will set up automatically
- `make migrate_clean`  <- Cleaning the migrations
- `make migrate`        <- Collect all migration from each module and run the migrate
- `make native_migrate` <- Collect all migration from each module and run the migrate natively
- `make build_wpgo`     <- Build the wpgo image
- `make artifacts`      <- Bundling the production image to artifacts

## How to Contribute
All contributions are welcome to help improve WPGO.

Before submiting your Merge Request (MR) consider the following guidelines:
- Clone the project to your local machine
- Checkout your branch with name defer on your ticket number. Ex. you take the issues number 1, then checkout to 1 as your workspace
- Doing test on your local and should be success on ci/cd test to
- Make sure you have test all of your code
- Commit your code with `project name#issues number: your message here`. so we can easily track the code to the issues
- Push your branch and do Merge Request if you have done
- Wait for the review, and your is qualified to merged ✅