FROM alpine

ARG VERSION_TAG
ARG GOPROXY
ARG GIN_MODE

ENV GOPROXY=$GOPROXY
ENV VERSION_TAG=$VERSION_TAG
ENV GIN_MODE=$GIN_MODE

RUN apk add --update ca-certificates bash

LABEL VERSION_TAG=$VERSION_TAG

COPY ./build/wpgo /api
COPY ./scripts/wait-for-it.sh /
COPY ./scripts/start.sh /

RUN chmod +x /start.sh /wait-for-it.sh
ENTRYPOINT [ "/start.sh" ]