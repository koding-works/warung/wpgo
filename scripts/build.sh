#!/bin/sh
set -e
set -x

export GO111MODULE=on

apk add --update bash git binutils musl-dev

WORKDIR=/go/src/wpgo

echo "Collecting module..."
cp /host/go.* $WORKDIR

# Registering new WPGO module below for build 
# See the README.md for detail

cp -a /host/auth $WORKDIR
cp -a /host/posts $WORKDIR
cp -a /host/products $WORKDIR
cp -a /host/vendors $WORKDIR
cp -a /host/orders $WORKDIR
cp -a /host/utils $WORKDIR
cp -a /host/db $WORKDIR

# End of module

cd $WORKDIR/cmd/wpgo
CGO_ENABLED=0 GOOS=linux go build
cp wpgo /build
strip /build/wpgo
ls -lR
cd $WORKDIR
chmod 644 go.mod go.sum