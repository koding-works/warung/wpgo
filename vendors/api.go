package vendors

import (
	"database/sql"
	"log"
	"net/http"
	"strconv"
	"wpgo/utils"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
)

type VendorAPI struct {
	Router *gin.RouterGroup
	DB     *sql.DB
	Redis  *redis.Client
}

func (api *VendorAPI) Register() {
	api.Router.GET("/vendors", api.vendorsListRequest)

	log.Println("Vendors API Registered")
}

func (api *VendorAPI) vendorsListRequest(c *gin.Context) {
	var meta vendorsMeta
	var err error

	if meta.Limit, err = strconv.Atoi(utils.Sanitize(c.DefaultQuery("per_page", "10"))); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "invalid-limit-type"})
		return
	}
	if meta.Page, err = strconv.Atoi(utils.Sanitize(c.DefaultQuery("page", "1"))); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "invalid-page-type"})
		return
	}

	result, err := api.all(meta)
	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, result)
}
