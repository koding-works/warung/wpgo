package vendors

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
	"wpgo/db"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

var api *VendorAPI
var resp = httptest.NewRecorder()
var req *gin.Context
var route *gin.Engine

func TestInit(t *testing.T) {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	req, route = gin.CreateTestContext(resp)

	api = &VendorAPI{
		DB:    db.InitMysql(),
		Redis: db.InitRedis(),
	}
}

func TestVendorsListRequest(t *testing.T) {
	route.GET("/vendors", api.vendorsListRequest)

	req.Request, _ = http.NewRequest(http.MethodGet, "/vendors", nil)
	route.ServeHTTP(resp, req.Request)

	assert.Equal(t, http.StatusOK, resp.Result().StatusCode, "Expected 200 status code")

	var data map[string]interface{}

	res, _ := ioutil.ReadAll(resp.Body)
	err := json.Unmarshal(res, &data)
	assert.Equal(t, nil, err, "Expected no error")
	assert.Equal(t, true, len(data["vendors"].([]interface{})) > 0, "Expected got vendors")

	// Test per page
	req.Request, _ = http.NewRequest(http.MethodGet, "/vendors?per_page=20&page=1", nil)
	route.ServeHTTP(resp, req.Request)

	assert.Equal(t, http.StatusOK, resp.Result().StatusCode, "Expected 200 status code")

	res, _ = ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(res, &data)
	assert.Equal(t, nil, err, "Expected no error")
	assert.Equal(t, true, len(data["vendors"].([]interface{})) > 0, "Expected got vendors")

}
