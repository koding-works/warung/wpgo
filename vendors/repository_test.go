package vendors

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAllVendors(t *testing.T) {
	meta := vendorsMeta{
		Limit: 10,
		Page:  1,
	}

	results, err := api.all(meta)
	assert.Equal(t, nil, err, "Expected no error")
	assert.Equal(t, true, len(results.Data) > 0, "Expected got result")

}
